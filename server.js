const express = require('express')
const cors = require('cors')
const app = express()
const db = require('./models');
app.use(express.json())
app.use('/images', express.static("./images"))
app.use(cors())

app.get('/product', async (req, res) => {
  try {
      const result = await db.Products.findAll({
        order: [
          ["id" , "DESC"]
        ]
      })
      res.status(200).json(result)
  } catch (error) {
      res.status(500).json({ message: error.message })
  }
})

app.get('/product/:id', async (req, res) => {
  try {
    const result = await db.Products.findOne({
      where: { "id" : req.params.id }
    })
    if(result){
      res.status(200).json(result)
    }else{
      res.status(404).json({ message: 'Product not found!!' })
    }
} catch (error) {
    res.status(500).json({ message: error.message })
}
})

app.post('/product', async (req, res) => {
  try {
    const product = await db.Products.create(req.body)
    res.status(201).json(product)
} catch (error) {
    res.status(500).json({ message: error.message })
}

})

app.put('/product/:id', async (req, res) => {
  try {
    const result = await db.Products.findOne({
      where: { "id" : req.params.id }
    })
    if(!result){
      return res.status(404).json({ message: 'Product not found!!' })
    }

    const product = await db.Products.update(req.body, {
      where: { "id" : result.id }
    })
    if([product]){
      const updateProduct = await db.Products.findByPk(result.id)
      res.status(200).json(updateProduct)
    }else{
      throw new Error('update product fail !!')
    }
} catch (error) {
    res.status(500).json({ message: error.message })
}
})

app.delete('/product/:id', async (req, res) => {
  try {
    const deleted = await db.Products.destroy({
      where: { "id" : req.params.id }
    })
    if(deleted){
      res.status(204).json({ message: 'product delete' })
    }else{
      res.status(404).json({ message: 'Product not found!!' })
    }
} catch (error) {
    res.status(500).json({ message: error.message })
}
})


const PORT = process.env.PORT || 3000
const ENV = process.env.NODE_ENV || 'development'

app.listen(PORT,()=>{
  console.log("PORT " + PORT)
  console.log("ENV " + ENV)
  console.log("server start")
})