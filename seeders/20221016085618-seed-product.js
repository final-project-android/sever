'use strict';

const { query } = require('express');
const { Sequelize } = require('sequelize');

/** @type {import('sequelize-cli').Migration} */
module.exports = {
  up: (queryInterface, Sequelize) => {

    let dateNow = new Date()
    DATA.map(item => {
      item.created_at = dateNow
      item.updated_at = dateNow
    })

    return queryInterface.bulkInsert(TABLE, DATA, {});
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.bulkDelete(TABLE, null, {});
  }
};

const TABLE = "Products"
const DATA = [
  {
    "name": "product1",
    "image": "product_10.jpg",
    "stock": 13,
    "price": 150
  },
  {
    "name": "product2",
    "image": "product_02.jpg",
    "stock": 1,
    "price": 185
  },
  {
    "name": "product3",
    "image": "product_01.jpg",
    "stock": 1,
    "price": 185
  },
  {
    "name": "product4",
    "image": "product_07.jpg",
    "stock": 1,
    "price": 185
  },
  {
    "name": "product5",
    "image": "product_05.jpg",
    "stock": 100,
    "price": 200
  },
  {
    "name": "product6",
    "image": "product_23.jpg",
    "stock": 2,
    "price": 130
  },
  {
    "name": "product7",
    "image": "product_12.jpg",
    "stock": 100,
    "price": 60
  },
  {
    "name": "product8",
    "image": "product_21.jpg",
    "stock": 100,
    "price": 370
  },
  {
    "name": "product9",
    "image": "product_04.jpg",
    "stock": 1000,
    "price": 300
  },
  {
    "name": "product10",
    "image": "product_16.jpg",
    "stock": 1000,
    "price": 300
  },
  {
    "name": "product11",
    "image": "product_17.jpg",
    "stock": 1000,
    "price": 300
  },
  {
    "name": "product12",
    "image": "product_19.jpg",
    "stock": 60,
    "price": 100
  },
  {
    "name": "product13",
    "image": "product_20.jpg",
    "stock": 100,
    "price": 290
  },
  {
    "name": "product14",
    "image": "product_14.jpg",
    "stock": 1000,
    "price": 300
  },
  {
    "name": "product15",
    "image": "product_15.jpg",
    "stock": 1000,
    "price": 300
  },
  {
    "name": "product16",
    "image": "product_11.jpg",
    "stock": 100,
    "price": 150
  },
  {
    "name": "product17",
    "image": "product_22.jpg",
    "stock": 0,
    "price": 280
  }
]
